package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDto;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractUserResponse {

    public UserViewProfileResponse(@Nullable final UserDto user) {
        super(user);
    }

}
