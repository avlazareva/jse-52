package ru.t1.lazareva.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable String... args) throws InterruptedException {
        Thread.sleep(System.getenv().containsKey("LATENCY") ? Integer.valueOf(System.getenv("LATENCY")) : 10000);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
