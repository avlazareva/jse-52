package ru.t1.lazareva.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ILoggerService {

    void writeLog(@NotNull final String message);

}