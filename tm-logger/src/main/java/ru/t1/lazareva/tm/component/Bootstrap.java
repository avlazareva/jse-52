package ru.t1.lazareva.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.api.IReceiverService;
import ru.t1.lazareva.tm.listener.LoggerListener;
import ru.t1.lazareva.tm.service.ReceiverService;

@NoArgsConstructor
public final class Bootstrap {

    @SneakyThrows
    public void start() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
        factory.setBrokerURL(getUrl());
        factory.setConnectResponseTimeout(30000);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receive(new LoggerListener());
    }

    private String getUrl() {
        if (System.getenv().containsKey("JMS_BROKERURL")) return System.getenv("JMS_BROKERURL");
        return "tcp://alpha:61616";
    }

}