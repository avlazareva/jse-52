package ru.t1.lazareva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    Boolean isLoginExists(@NotNull String login) throws Exception;

    Boolean isEmailExists(@NotNull String email) throws Exception;

}