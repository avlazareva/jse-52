package ru.t1.lazareva.tm.api.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    EntityManagerFactory getEMFactory();

    EntityManager getEntityManager();

}
