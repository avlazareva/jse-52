package ru.t1.lazareva.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.lazareva.tm.api.service.IServiceLocator;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.dto.model.SessionDto;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.lazareva.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectDtoService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        try {
            @NotNull final ProjectDto project = getProjectService().changeProjectStatusById(userId, id, status);
            return new ProjectChangeStatusByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        try {
            @NotNull final ProjectDto project = getProjectService().changeProjectStatusByIndex(userId, index, status);
            return new ProjectChangeStatusByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        try {
            getProjectService().clear(userId);
            return new ProjectClearResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        try {
            @NotNull final ProjectDto project = getProjectService().changeProjectStatusById(userId, id, Status.COMPLETED);
            return new ProjectCompleteByIdResponse();
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIndexResponse completeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        try {
            @NotNull final ProjectDto project = getProjectService().changeProjectStatusByIndex(userId, index, Status.COMPLETED);
            return new ProjectCompleteByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @NotNull final ProjectDto project = getProjectService().create(userId, name, description);
            return new ProjectCreateResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSortType();
        try {
            @NotNull final List<ProjectDto> projects = getProjectService().findAll(userId, sort);
            return new ProjectListResponse(projects);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getProjectService().removeById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        try {
            getProjectService().removeByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new ProjectRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            @Nullable final ProjectDto project = getProjectService().findOneById(userId, id);
            return new ProjectShowByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        try {
            @Nullable final ProjectDto project = getProjectService().findOneByIndex(userId, index);
            return new ProjectShowByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        try {
            @Nullable final ProjectDto project = getProjectService().changeProjectStatusById(userId, id, Status.IN_PROGRESS);
            return new ProjectStartByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIndexResponse startProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        try {
            @Nullable final ProjectDto project = getProjectService().changeProjectStatusByIndex(userId, index, Status.IN_PROGRESS);
            return new ProjectStartByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }

    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @Nullable final ProjectDto project = getProjectService().updateById(userId, id, name, description);
            return new ProjectUpdateByIdResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request) {
        @NotNull SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        try {
            @Nullable final ProjectDto project = getProjectService().updateByIndex(userId, index, name, description);
            return new ProjectUpdateByIndexResponse(project);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

}
