package ru.t1.lazareva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.lazareva.tm.dto.model.AbstractUserOwnedModelDto;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> implements IUserOwnedDtoRepository<M> {

    @NotNull
    final static String HINT = "org.hibernate.cacheable";

    public AbstractUserOwnedDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setHint(HINT, true)
                .getResultList();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

    @Override
    public boolean existsByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        return findOneByIndex(userId, index) != null;
    }

    @Nullable
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator comparator) throws Exception {
        @NotNull String query = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        @NotNull String orderStatement = " ORDER BY m." + getSortType(comparator);
        query += orderStatement;
        return entityManager.createQuery(query, getEntityClass())
                .setParameter("userId", userId)
                .setHint(HINT, true)
                .getResultList();
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .setHint(HINT, true)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String jpql = "SELECT m FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId AND m.index = :index";
        return entityManager.createQuery(jpql, getEntityClass())
                .setParameter("userId", userId)
                .setParameter("index", index)
                .setMaxResults(1)
                .setHint(HINT, true)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getEntityClass().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        Optional<M> modelResult = Optional.ofNullable(findOneById(userId, model.getId()));
        modelResult.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull String query = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.id = :id AND m.userId = :userId";
        entityManager.createQuery(query).setParameter("id", id).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull String query = "DELETE FROM " + getEntityClass().getSimpleName() + " m WHERE m.index = :index AND m.userId = :userId";
        entityManager.createQuery(query).setParameter("index", index).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public M update(@NotNull final String userId, @NotNull final M model) throws Exception {
        model.setUserId(userId);
        return entityManager.merge(model);
    }

}

